package handler

import (
	"angalamman/dal"
	"angalamman/helper"
	"angalamman/helper/response"
	"angalamman/model"
	"encoding/json"
	"fmt"
	"net/http"
)

// UserHandler ... handler instance
type UserHandler struct {
	UserDal dal.IUserDal
	Res     response.WithStatus
}

// Createuser ... Create user
func (u UserHandler) Createuser(w http.ResponseWriter, r *http.Request) {

	userModel := &model.UserModel{}

	if err := json.NewDecoder(r.Body).Decode(&userModel); err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}
	fmt.Println("--password", userModel.Password)
	// userModel.
	// var
	userModel.Password, _ = helper.HashPassword(userModel.Password)
	err := u.UserDal.CreateUser(userModel)

	fmt.Println("--password", userModel.Password)

	if err != nil {

		fmt.Fprintf(w, err.Error())
		return
	}

	u.Res.With200m(w, "User Success Fully Registered", nil)(http.StatusOK)
	return
}

// UserLogin ...user Login
func (u UserHandler) UserLogin(w http.ResponseWriter, r *http.Request) {

	userModel := &model.UserModel{}

	if err := json.NewDecoder(r.Body).Decode(&userModel); err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}

	isCheck := u.UserDal.Login(userModel.Email, userModel.Password)
	if isCheck {
		u.Res.With200m(w, "you have Logged In Successsfully", nil)(http.StatusOK)
		return
	} else {
		u.Res.With200m(w, "Please Provide a valid Email and Password", nil)(500)
		return
	}

}

// Viewuser ...
func (u UserHandler) Viewuser(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintf(w, "check for api")
	return
}
