package response

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// JSONFormat ...
type JSONFormat struct {
	Error   bool        `json:"error"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// WithStatus ... http Status
type WithStatus struct {
}

// With200m ... http Response with proper Json Format
func (ws WithStatus) With200m(w http.ResponseWriter, message string, data interface{}) func(int) {

	jsonData := JSONFormat{}

	jsonData.Message = message
	jsonData.Data = data

	return func(statusCode int) {
		if statusCode != 200 {

			jsonData.Error = true
		}

		dataB, err := json.Marshal(jsonData)

		w.Header().Set("Content-Type", "application/json")
		if err != nil {
			w.WriteHeader(422)
			fmt.Fprintf(w, "Invalid Data")
			return
		}
		w.WriteHeader(statusCode)
		w.Write(dataB)
		return
	}
}
