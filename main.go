package main

import (
	"angalamman/dal"
	"angalamman/handler"
	"angalamman/helper/response"
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load("./config/config.env")

	if err != nil {
		panic(err)
	}
	port := os.Getenv("port")
	fmt.Println("----port", port)
	http.ListenAndServe(port, Routes())

}

// Routes ...Api endPoint Router
func Routes() *mux.Router {
	r := mux.NewRouter()

	handlerInst := handler.UserHandler{
		UserDal: &dal.UserDal{}, Res: response.WithStatus{}}
	// r.PathPrefix("/api/v1")

	r.HandleFunc("/user", handlerInst.Createuser).Methods("POST")
	r.HandleFunc("/user", handlerInst.Viewuser).Methods("GET")
	r.HandleFunc("/user/login", handlerInst.UserLogin).Methods("POST")

	return r

}
