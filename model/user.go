package model

// UserCollection ...schema Name
const (
	UserCollection = "user"
)

// UserModel ... Data Model for user Collection
type UserModel struct {
	FirstName string `json:"firstName" bson:"firstName"`
	LastName  string `json:"lastName" bson:"lastName"`
	Mobile    int    `json:"mobile" bson:"mobile"`
	Email     string `json:"email" bson:"email"`
	Password  string `json:"password" bson:"password"`
	Address   string `json:"address" bson:"address"`
	PinCode   string `json:"pinCode" bson:"pinCode"`
}
