package dal

import (
	"angalamman/db"
	"angalamman/model"

	"angalamman/helper"

	"gopkg.in/mgo.v2/bson"
)

// IUserDal ...
type IUserDal interface {
	CreateUser(user *model.UserModel) error
	Login(email, passwordHash string) bool
}

// UserDal ...mongoQuery For User Layer
type UserDal struct {
}

func (u *UserDal) CreateUser(user *model.UserModel) error {

	mongdb, session := db.ConnectMongoDb()
	defer session.Close()
	col := mongdb.C(model.UserCollection)
	return col.Insert(user)

}

func (u *UserDal) Login(email, password string) bool {
	mongdb, session := db.ConnectMongoDb()
	defer session.Close()
	col := mongdb.C(model.UserCollection)
	var userDetails *model.UserModel
	err := col.Find(bson.M{"email": email}).One(&userDetails)
	if err != nil {
		return false
	}
	return helper.CheckPasswordHash(password, userDetails.Password)
}
