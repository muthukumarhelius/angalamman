package db

import (
	"log"

	"os"

	mgo "gopkg.in/mgo.v2"
)

// ConnectMongoDb ... MongoDb Instance
func ConnectMongoDb() (*mgo.Database, *mgo.Session) {

	log.Println("connecting mongo......")
	uri := os.Getenv("MONGODB_URL")

	mgoSession, err := mgo.Dial(uri)

	if err != nil {
		panic(err)
	}
	databaseName := os.Getenv("DBNAME")

	db := mgoSession.DB(databaseName)

	return db, mgoSession

}
